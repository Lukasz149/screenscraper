var tabUrl = '';
var screenScraper = {
    steps: [],
    isRecording: false
};

chrome.tabs.getSelected(null, function(tab) {
    tabUrl = tab.url;
    $('#page-url').text(tabUrl);
});

$('document').ready(function() {
    var requestsResult = [];
    init();
    $('#record-button').click(function (event) {
        if (event !== 'undefined' && window.getSelection().toString() === '') {
            changeRecordMode();
            saveScreenScraper();
            console.log(screenScraper.steps);
        }
    });

    $('#remove-button').click(function () {
        screenScraper = {
            steps: [],
            isRecording: false
        };
        saveScreenScraper();
        var recordIcon = $('#record-icon');
        changeRecordButtonToStop(recordIcon);
        validate();
    });

    function getContentAfterClick(temporaryData) {
        var promises = [];
        for( var j = 0; j < temporaryData.length; j++) {
            var tmpClass = '.' + screenScraper.steps[stepsCounter].class.replace(/\s\s+/g, ' ').replace(/\s/g, '.').replace(/\.$/, "");
            var tmp = $(temporaryData[j]).find(tmpClass);
            for(var k = 0; k < tmp.length; k++) {
                var promise = $.ajax({
                    type : 'GET',
                    url: tmp[k].href,
                    dataType: "html",
                    success: function(data) {
                        requestsResult.push(data);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
                promises.push(promise);
            }
        }
        return promises;
    }
    var promises = [];
    var temporaryData = [];
    var stepsCounter = 0;
    var generatedData = [];
    var dataCounter = 0;

    function resolveClass(className) {
        return '.' + className.replace(/\s\s+/g, ' ').replace(/\s/g, '.').replace(/\.$/, "");
    }

    function resolveElementText(data, element) {
        if(element.id !== '') {
            var elementId = element.id;
            return $(data).find(elementId)[0].innerText;
        } else if(element.class !== '') {
            var elementClass = resolveClass(element.class);
            var elements = $(data).find(elementClass);
            if(elements.length > 0 && element.elementIndex !== '') {
                return elements[element.elementIndex].innerText;
            }
        }
    }

    function generateData(temporaryData) {
        stepsCounter++;
        if(stepsCounter < screenScraper.steps.length) {
            var step = screenScraper.steps[stepsCounter];
            if (step.event === 'klik' && step.href.startsWith('http')) {
                promises = getContentAfterClick(temporaryData);
                resolvePromises(promises);
            } else if (step.event === 'klik') {
                generateData(temporaryData);
            } else if (step.event === 'wybór') {
                var tmpData = [];
                for (var l = 0; l < temporaryData.length; l++) {
                    var text = resolveElementText(temporaryData[l], step);
                    text = text.replace(/(\r\n|\n|\r)/gm," ");
                    tmpData.push(text);
                    if(typeof generatedData[l] !== 'undefined') {
                        generatedData[l].push(text);
                    } else {
                        generatedData[l] = [];
                        generatedData[l].push(text);
                    }
                }
                dataCounter++;
                generateData(temporaryData)
            }
        } else if(stepsCounter === screenScraper.steps.length) {
            downloadCSV();
        }
    }

    function resolvePromises(promises) {
        $.when.apply(undefined, promises).then(function() {
            temporaryData = requestsResult;
            requestsResult = [];
            for(var i = 0; i < temporaryData.length; i++) {
                generatedData[i] = [];
            }
            generateData(temporaryData);
        });
    }

    $('#generate-button').click(function (event) {
        var request = new XMLHttpRequest();

        request.open("POST", "http://localhost:8080/data", true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.setRequestHeader("Access-Control-Allow-Origin", "*");
        request.send(JSON.stringify(screenScraper.steps));

        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    window.open("http://localhost:8080", '_blank') ;
                } else {
                    console.log('failed');
                }
            }
        };
        console.log("Generowanie danych...");
        var startedPage = "";
        var temporaryData = [];
    });

    function convertArrayOfObjectsToCSV(data) {
        var result, ctr, columnDelimiter, lineDelimiter;

        if (data === null || !Object.keys(data).length) {
            return null;
        }

        columnDelimiter = '\t';
        lineDelimiter = '\n';

        result = '';

        data.forEach(function(item) {
            ctr = 0;
            item.forEach(function(value) {
                if (ctr > 0) {
                    result += columnDelimiter;
                }
                result += value;
                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }

    function downloadCSV() {
        console.log("Rozpoczynam pobieranie...");
        var data, filename, link;
        console.log(generatedData);

        var csv = convertArrayOfObjectsToCSV(generatedData);
        if (csv === null) return;

        filename = 'export.csv';

        /*if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }*/
        data = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.innerText = filename;
        //link.click();
        $('#generate-button').parent().append(link);

        var blob = new Blob([csv], {type: "text/csv"});
        var url = URL.createObjectURL(blob);
        chrome.downloads.download({
            url: url, // The object URL can be used as download URL
            filename: filename
        });

        console.log("Pobieranie zakończone!");

    }
});

function validate() {
    var validate = false;
    $.each(screenScraper.steps, function(idx, element) {
        if(element.event === "wybór") {
            validate = true;
        }
    });

    if(validate) {
        $('#generate-button').attr("disabled", false);
    } else {
        $('#generate-button').attr("disabled", true);
    }
}

function init() {
    getSavedScreenScraper('screen-scraper', function(ss) {
        if(ss) {
            screenScraper = JSON.parse(atob(ss));
            var recordIcon = $('#record-icon');
            if(screenScraper.isRecording) {
                changeRecordButtonToRecording(recordIcon)
            } else {
                changeRecordButtonToStop(recordIcon);
            }
            generateStepsTable();
            validate();
        }
    });
}

function saveScreenScraper() {
    chrome.storage.sync.set({
        'screen-scraper': btoa(JSON.stringify(screenScraper))
    });
}

function getSavedScreenScraper(url, callback) {
    chrome.storage.sync.get(url, function(items) {
        callback(chrome.runtime.lastError ? null : items[url]);
    });
}

function changeRecordMode() {
    var recordIcon = $('#record-icon');
    if (screenScraper.isRecording) {
        screenScraper.isRecording = false;
        changeRecordButtonToStop(recordIcon);
    } else {
        screenScraper.isRecording = true;
        changeRecordButtonToRecording(recordIcon);
        screenScraper.steps.push({
            initUrl: tabUrl
        });
    }
}

function changeRecordButtonToRecording(recordIcon) {
    recordIcon.removeClass('fa-play');
    recordIcon.addClass('fa-stop');
}

function changeRecordButtonToStop(recordIcon) {
    recordIcon.removeClass('fa-stop');
    recordIcon.addClass('fa-play');
}

chrome.storage.onChanged.addListener(function(changes, namespace) {
    if(changes.hasOwnProperty('screen-scraper')) {
        var storageChange = changes['screen-scraper'];
        screenScraper = JSON.parse(atob(storageChange.newValue));
        generateStepsTable();
        validate();
    }
});

function generateStepsTable() {
    var stepsTableBody = $('#steps-table-body');
    stepsTableBody.empty();
    var tdLp = $("<td></td>").text(1);
    var tdEvent = $("<td></td>").text("start");
    var tdHref = $("<td></td>").text(screenScraper.steps[0].initUrl);
    var trr = $("<tr></tr>");
    trr.append(tdLp, tdEvent, tdHref);
    stepsTableBody.append(trr);
    for (var i = 1; i < screenScraper.steps.length; i++) {
        if(screenScraper.steps[i].hasOwnProperty('initUrl')) {
            var td1 = $("<td></td>").text(i + 1);
            var td2 = $("<td></td>").text("start");
            var td3 = $("<td></td>").text(screenScraper.steps[i].initUrl);
            var td6 = $("<td></td>").text(screenScraper.steps[i].id);
            var button = $("<button type=\"button\" class=\"btn btn-danger btn-sm delete-element-button\" >\n" +
                "<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n" +
                "</button>");
            $(button).attr( "data-index", i);
            var td4 = $("<td></td>").append(button);
            var tr = $("<tr></tr>");
            tr.append(td1, td2, td3, td6, td4);
            stepsTableBody.append(tr);
        } else {
            var td1 = $("<td></td>").text(i + 1);
            var td2 = $("<td></td>").text(screenScraper.steps[i].event);
            var td3 = $("<td></td>").text(screenScraper.steps[i].href);
            var td6 = $("<td></td>").text(screenScraper.steps[i].name);
            var button = $("<button type=\"button\" class=\"btn btn-danger btn-sm delete-element-button\" >\n" +
                "<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n" +
                "</button>");
            $(button).attr( "data-index", i);
            var td4 = $("<td></td>").append(button);
            var tr = $("<tr></tr>");
            tr.append(td1, td2, td3, td6, td4);
            stepsTableBody.append(tr);
        }

    }

    $('.delete-element-button').click(function () {
        deleteElement($(this).attr("data-index"));
    });
}

function deleteElement(i) {
    screenScraper.steps.splice(i, 1);
    saveScreenScraper();
    generateStepsTable();
    validate();
}