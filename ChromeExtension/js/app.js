var screenScraper = {
  steps: [],
  isRecording: false
};

$('document').ready(function() {
    init();
    document.addEventListener('mouseup', onMouseUpListener);
    document.addEventListener('click', clickListener);
});

/*$("#data-add").click(function (event) {
    event.preventDefault();
    tmpElement.name = $("#data-name").val();
    addNewStep();
    $(this).parent('div').remove();
});*/

var tmpElement;
var onMouseUpListener = function(event){
    if(screenScraper.isRecording && event!== 'undefined' && window.getSelection().toString() !== '') {
        var index = getElementIndex(event.srcElement);
        tmpElement = {
            event: 'wybór',
            element: event.srcElement,
            class: event.srcElement.className,
            id: event.srcElement.id,
            href: '',
            elementIndex: index,
            name: ''
        };

        $(event.srcElement).append('<div style="position: absolute"><input type="text" id="data-name" style="height: 30px; padding: 9px 8px;"/>' +
            '<button type="button" id="data-add" style="background: #2eb82e; color: white; height: 30px; font-size: 14px; border-bottom-right-radius: 5px; border-top-right-radius: 5px;">Dodaj</button>' +
            '</div>');


        /*screenScraper.steps.push({
            event: 'wybór',
            element: event.srcElement,
            class: event.srcElement.className,
            id: event.srcElement.id,
            href: '',
            elementIndex: index
        });
        saveScreenScraper();*/
    }
};

function addNewStep() {
    screenScraper.steps.push(tmpElement);
    saveScreenScraper();
}

function getElementIndex(element) {
    var index = '';
    if(element.className !== '') {
        var elements = $('body').find(resolveClass(element.className));
        $.map(elements, function(obj, idx) {
            if(obj.innerText === element.innerText) {
                index = idx;
            }
        });
    }
    return index;
}
function resolveClass(className) {
    return '.' + className.replace(/\s\s+/g, ' ').replace(/\s/g, '.');
}
var clickListener = function (event) {
    if(screenScraper.isRecording && event!== 'undefined' && window.getSelection().toString() === '' && event.srcElement.id !== 'record-button' && event.srcElement.id !== 'record-icon'
        && event.srcElement.id !== 'data-name' && event.srcElement.id !== 'data-add') {
        screenScraper.steps.push({
            event: 'klik',
            element: event.srcElement,
            class: event.srcElement.className,
            id: event.srcElement.id,
            href: event.srcElement.href,
            elementIndex: '',
            name: ''
        });
        saveScreenScraper();
    } else if(event.srcElement.id === 'data-add') {
        event.preventDefault();
        tmpElement.name = $("#data-name").val();
        addNewStep();
        $(event.srcElement).parent('div').remove();
    }
};

var saveScreenScraper = function() {
    chrome.storage.sync.set({
        'screen-scraper': btoa(JSON.stringify(screenScraper))
    });
};

var init = function() {
    getSavedScreenScraper('screen-scraper', function(ss) {
        if (ss) {
            screenScraper = JSON.parse(atob(ss));
        }
    });
};

function getSavedScreenScraper(name, callback) {
    chrome.storage.sync.get(name, function(items) {
        callback(chrome.runtime.lastError ? null : items[name]);
    });
}

chrome.storage.onChanged.addListener(function(changes, namespace) {
    if(changes.hasOwnProperty('screen-scraper')) {
        var storageChange = changes['screen-scraper'];
        screenScraper = JSON.parse(atob(storageChange.newValue));
    }
});