# Screen scraper #

Screen scraper to wtyczka do przeglądarki Chrome pozwalająca na automatyczne pobranie interesujących użytkownika danych z aktualnie odwiedzanej strony internetowej.

Wtyczka umożliwia nagrywanie kroków wykonanych na stronie www przez użytkownika.

Nagrywanie obsługuje dwa typy zdarzeń:

*	kliknięcie w link - zapisywany jest adres strony na którą następuje przekierowanie oraz atrybuty pozwalające określić ilość wystąpień klikniętego elementu w celu generacji danych dla wszystkich wystąpień
*	zaznaczenie - poprzez zaznaczenie interesującego nas tekstu lub jego fragmentu zapisywane są odpowiednie atrybuty zaznaczonego elementu w celu jednoznacznego określenia interesujących użytkownika danych

Po wykonaniu nagrywania możliwe jest wygenerowanie danych.

Program przechodząc przez wszystkie kroki pobiera źródła stron dostępne pod adresem URL klikniętego elementu oraz zapisuje odpowiednie dane tekstowe określone przez użytkownika poprzez zaznaczenie.

Po zakończeniu generowania danych następuje tworzenie pliku .csv zawierającego dane tekstowe. Plik zostaje automatycznie pobrany na komputer użytkownika.