package screenscraper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import screenscraper.model.UserAccount;
import screenscraper.service.IUserService;

import java.security.Principal;

@Controller
public class UserController {
    private IUserService userService;


    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }


    @RequestMapping("/")
    public String getUserPage(Model model, Authentication auth) {

        if(auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
            UserAccount userAccount = userService.findUserAccountByEmail(auth.getName());
            model.addAttribute("user", userAccount);
            return "index";
        } else {
            System.out.println("Model:" + model.toString());
            return "index";
        }
    }

    @RequestMapping("/login")
    public String getLoginPage(Model model) {

        System.out.println("Model:" + model.toString());

        return "login";
    }

    @RequestMapping("/dashboard")
    public String getDashboardPage(Model model, Principal principal) {

        System.out.println("Model:" + model.toString());
        System.out.println("Principal: " + principal.toString());

        UserAccount userAccount = userService.findUserAccountByEmail(principal.getName());
        model.addAttribute("user", userAccount);

        return "dashboard";
    }
}

