package screenscraper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import screenscraper.model.RegisterUserRequest;
import screenscraper.model.UserAccount;
import screenscraper.service.IUserService;
import screenscraper.validator.UserAccountValidator;

import javax.validation.Valid;
import java.util.List;

@RestController
public class RegisterUserController {
    private IUserService registerUserService;
    private UserAccountValidator userAccountValidator;

    @Autowired
    public RegisterUserController(IUserService registerUserService,
                                  UserAccountValidator userAccountValidator) {
        this.registerUserService = registerUserService;
        this.userAccountValidator = userAccountValidator;
    }

    @RequestMapping(value = "/registerUser",consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ObjectError> registerUser(@Valid @RequestBody RegisterUserRequest registerUserRequest,
                                          BindingResult bindingResult,
                                          SecurityContextHolderAwareRequestWrapper auth) {

        System.out.println(registerUserRequest);
        UserAccount userAccount = new UserAccount(
                registerUserRequest.getEmail(),
                registerUserRequest.getPassword(),
                registerUserRequest.getPasswordConfirm(),
                registerUserRequest.getRole()
        );

        userAccountValidator.validate(userAccount, bindingResult);

        if(bindingResult.hasErrors()) {
            return bindingResult.getAllErrors();
        } else {
            registerUserService.registerUser(userAccount);
        }
        return null;
    }
}
