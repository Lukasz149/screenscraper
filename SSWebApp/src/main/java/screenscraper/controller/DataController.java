package screenscraper.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import screenscraper.model.GeneratingDataSteps;
import screenscraper.model.UserAccount;
import screenscraper.service.IUserService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DataController {
    public String steps;

    private IUserService userService;


    @Autowired
    public DataController(IUserService userService) {
        this.userService = userService;
    }

    @CrossOrigin
    @RequestMapping(value = "/data", method =  RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getData(@RequestBody String data) {
        System.out.println(data);
        this.steps = data;
        return new ResponseEntity<String>("Hello World", HttpStatus.OK);
    }

    @RequestMapping(value = "/data", method =  RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getData() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        return new ResponseEntity<String>(this.steps, responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "/getData", method =  RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GeneratingDataSteps> getDataById(@RequestParam(value = "id", required = true) Integer id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        GeneratingDataSteps generatingDataSteps = userService.getDataById(id);

        return new ResponseEntity<GeneratingDataSteps>(generatingDataSteps, responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllUserData", method =  RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GeneratingDataSteps>> getAllUserData(Authentication auth) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        if(auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
            UserAccount userAccount = userService.findUserAccountByEmail(auth.getName());
            List<GeneratingDataSteps> allGeneratingDataSteps = userService.getAllUserData(userAccount.getId());
            return new ResponseEntity<List<GeneratingDataSteps>>(allGeneratingDataSteps, responseHeaders,  HttpStatus.OK);
        } else {
            return new ResponseEntity<List<GeneratingDataSteps>>(new ArrayList<>(), responseHeaders, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/saveData", method =  RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDataById(Authentication auth, @RequestBody GeneratingDataSteps data) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Access-Control-Allow-Origin", "*");
        if(auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
            UserAccount userAccount = userService.findUserAccountByEmail(auth.getName());
            data.setUserId(userAccount.getId());
            userService.saveData(data);
            return new ResponseEntity<String>(responseHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(responseHeaders, HttpStatus.FORBIDDEN);
        }
    }
}
