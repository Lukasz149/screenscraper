package screenscraper.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import screenscraper.model.UserAccount;
import screenscraper.service.UserService;

@Component
public class UserAccountValidator implements Validator {
    private UserService userService;

    @Autowired
    public UserAccountValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserAccount.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserAccount user = (UserAccount) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getEmail().length() < 6 || user.getEmail().length() > 32) {
            errors.rejectValue("email", "Podany email jest nieprawidłowy");
        }
        if (userService.findUserAccountByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "Podany email jest już zarejestrowany");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Hasło musi zawierać minimum 8 znaków");
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            errors.rejectValue("passwordConfirm", "Podane hasła nie są takie same");
        }
    }
}