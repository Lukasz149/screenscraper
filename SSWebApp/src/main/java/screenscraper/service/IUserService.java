package screenscraper.service;

import screenscraper.model.GeneratingDataSteps;
import screenscraper.model.UserAccount;

import java.util.List;

public interface IUserService {
    void registerUser(UserAccount userAccount);
    UserAccount findUserAccountByEmail(String email);
    GeneratingDataSteps getDataById(Integer id);

    void saveData(GeneratingDataSteps data);

    List<GeneratingDataSteps> getAllUserData(int id);
}
