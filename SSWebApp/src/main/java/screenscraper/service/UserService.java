package screenscraper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import screenscraper.dao.IUserDao;
import screenscraper.model.GeneratingDataSteps;
import screenscraper.model.UserAccount;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserService implements IUserService, UserDetailsService {

    private IUserDao userDao;
    /*@Autowired
    private AuthenticationManager authenticationManager;*/

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void registerUser(UserAccount userAccount) {
        userAccount.setPassword(bCryptPasswordEncoder.encode(userAccount.getPassword()));
        userAccount.setPasswordConfirm(bCryptPasswordEncoder.encode(userAccount.getPasswordConfirm()));
        userDao.saveUserAccountInDatabase(userAccount);
    }

    @Override
    public UserAccount findUserAccountByEmail(String email) {
        return userDao.findUserAccountByEmail(email);
    }

    @Override
    public GeneratingDataSteps getDataById(Integer id) {
        return userDao.getDataById(id);
    }

    @Override
    public void saveData(GeneratingDataSteps data) {
        userDao.saveData(data);
    }

    @Override
    public List<GeneratingDataSteps> getAllUserData(int id) {
        return userDao.getAllUserData(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccount userAccount = findUserAccountByEmail(username);
        if(userAccount!= null) {
            Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
            grantedAuthorities.add(new SimpleGrantedAuthority(userAccount.getRole()));
            return new org.springframework.security.core.userdetails.User(userAccount.getEmail(), userAccount.getPassword(), grantedAuthorities);
        }
        return null;
    }
}
