package screenscraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScreenscraperApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScreenscraperApplication.class, args);
	}
}
