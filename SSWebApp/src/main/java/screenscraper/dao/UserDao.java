package screenscraper.dao;

import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import screenscraper.model.GeneratingDataSteps;
import screenscraper.model.UserAccount;

import javax.persistence.EntityManagerFactory;
import java.util.List;

@Component
public class UserDao extends AbstractSimpleDao implements IUserDao {

    @Autowired
    public UserDao(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    @Transactional
    public void saveUserAccountInDatabase(UserAccount userAccount) {
        getSession().save(userAccount);

    }

    @Override
    @Transactional
    public UserAccount findUserAccountByEmail(String email) {
        return (UserAccount) getSession().createQuery("from UserAccount where email = :email")
                .setParameter("email", email)
                .uniqueResult();
    }

    @Override
    @Transactional
    public GeneratingDataSteps getDataById(Integer id) {
        return (GeneratingDataSteps) getSession().createQuery("from GeneratingDataSteps where id = :id")
                .setParameter("id", id)
                .uniqueResult();
    }

    @Override
    @Transactional
    public void saveData(GeneratingDataSteps data) {
        getSession().save(data);
    }

    @Override
    @Transactional
    public List<GeneratingDataSteps> getAllUserData(int id) {
        return (List<GeneratingDataSteps>) getSession().createQuery("from GeneratingDataSteps where userId = :id")
                .setParameter("id", id)
                .list();
    }
}

