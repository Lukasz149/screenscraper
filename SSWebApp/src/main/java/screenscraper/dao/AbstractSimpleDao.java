package screenscraper.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManagerFactory;

public class AbstractSimpleDao {
    private SessionFactory sessionFactory;

    public AbstractSimpleDao(EntityManagerFactory entityManagerFactory) {
        if(entityManagerFactory.unwrap(SessionFactory.class) == null){
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    }

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

}

